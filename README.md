# Interval Timer

![screenshot](feature-background-counter-screenshot.png)

This example shows how to use the [`Worker`](https://developer.getpebble.com/docs/c/group___worker.html) (background worker).

Install apk on phone https://www.apkmirror.com/apk/pebble-technology-corp/pebble/pebble-4-4-2-1405-62d45d7d7-endframe-release/pebble-4-4-2-1405-62d45d7d7-endframe-android-apk-download/download/

Connect phone
Enable developer connection in app

download docker
https://hub.docker.com/r/dmorgan81/rebble/
docker pull dmorgan81/rebble
