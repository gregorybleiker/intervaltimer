#include "settings.h"

// Initialize the default settings
void prv_default_settings()
{
    settings.Timer[0] = atoi("120");
    settings.Timer[1] = atoi("120");
    settings.Timer[2] = atoi("120");
    strcpy(settings.Name[0], "one");
    strcpy(settings.Name[1], "two");
    strcpy(settings.Name[2], "three");
}

// Read settings from persistent storage
void prv_load_settings()
{
    // Load the default settings
    prv_default_settings();
    // Read settings from persistent storage, if they exist
    persist_read_data(SETTINGS_KEY, &settings, sizeof(settings));
}

// Save the settings to persistent storage
void prv_save_settings()
{
    persist_write_data(SETTINGS_KEY, &settings, sizeof(settings));
    // Update the display based on new settings
}
