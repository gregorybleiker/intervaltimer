module.exports = [
  {
    type: "heading",
    defaultValue: "Inteval timer",
  },
  {
    type: "text",
    defaultValue: "Set the length of the intervals",
  },
  {
    type: "section",
    items: [
      {
        type: "heading",
        defaultValue: "Intervals",
      },
      {
        type: "section",
        items: [
          {
            type: "heading",
            defaultValue: "Interval One",
          },
          {
            type: "input",
            messageKey: "NameOne",
            defaultValue: "One",
            label: "Name",
            attributes: {
              placeholder: "eg: Walk",
              type: "text",
            },
          },
          {
            type: "input",
            messageKey: "IntervalOne",
            defaultValue: "120",
            label: "Duration (s)",
            attributes: {
              placeholder: "eg: 120",
              type: "number",
            },
          },
        ],
      },
      {
        type: "section",
        items: [
          {
            type: "heading",
            defaultValue: "Interval Two",
          },
          {
            type: "input",
            messageKey: "NameTwo",
            defaultValue: "Two",
            label: "Name",
            attributes: {
              placeholder: "eg: Run",
              type: "text",
            },
          },
          {
            type: "input",
            messageKey: "IntervalTwo",
            defaultValue: "120",
            label: "Duration (s)",
            attributes: {
              placeholder: "eg: 120",
              type: "number",
            },
          },
        ],
      },
      {
        type: "section",
        items: [
          {
            type: "heading",
            defaultValue: "Interval Three",
          },
          {
            type: "input",
            messageKey: "NameThree",
            defaultValue: "Three",
            label: "Name",
            attributes: {
              placeholder: "eg: Rest",
              type: "text",
            },
          },
          {
            type: "input",
            messageKey: "IntervalThree",
            defaultValue: "120",
            label: "Duration (s)",
            attributes: {
              placeholder: "eg: 120",
              type: "number",
            },
          },
        ],
      },
    ],
  },
  {
    type: "submit",
    defaultValue: "Save Settings",
  },
];
