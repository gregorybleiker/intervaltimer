#pragma once

#include <pebble.h>

#define SETTINGS_KEY 1

// A structure containing our settings
typedef struct ClaySettings {
  int Timer[3];
  char Name[3][32]; 
} __attribute__((__packed__)) ClaySettings;

ClaySettings settings;

void prv_default_settings();
void prv_load_settings();
void prv_save_settings();
