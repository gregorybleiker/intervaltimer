#include "main.h"
#include "settings.h"

#define WORKER_TICKS 0

static Window *s_main_window;
static TextLayer *s_first_line, *s_second_line, *s_third_line;
static Layer *s_path_layer;

int remainingSeconds = 0;
int rounds = 1;
int currentInterval = 0;

// Handle the response from AppMessage
static void prv_inbox_received_handler(DictionaryIterator *iter, void *context)
{

  Tuple *first_interval_t = dict_find(iter, MESSAGE_KEY_IntervalOne);
  if (first_interval_t)
  {
    settings.Timer[0] = atoi(first_interval_t->value->cstring);
  }

  Tuple *second_interval_t = dict_find(iter, MESSAGE_KEY_IntervalTwo);
  if (second_interval_t)
  {
    settings.Timer[1] = atoi(second_interval_t->value->cstring);
  }

  Tuple *third_interval_t = dict_find(iter, MESSAGE_KEY_IntervalThree);
  if (third_interval_t)
  {
    settings.Timer[2] = atoi(third_interval_t->value->cstring);
  }

  Tuple *name_one_t = dict_find(iter, MESSAGE_KEY_NameOne);
  if (name_one_t)
  {
    strcpy(settings.Name[0], name_one_t->value->cstring);
  }

  Tuple *name_two_t = dict_find(iter, MESSAGE_KEY_NameTwo);
  if (name_two_t)
  {
    strcpy(settings.Name[1], name_two_t->value->cstring);
  }

  Tuple *name_three_t = dict_find(iter, MESSAGE_KEY_NameThree);
  if (name_three_t)
  {
    strcpy(settings.Name[2], name_three_t->value->cstring);
  }
  // Save the new settings to persistent storage
  prv_save_settings();
  reset_interval_display();
}

static void worker_message_handler(uint16_t type, AppWorkerMessage *data)
{
  if (type == WORKER_TICKS)
  {
    static char s_activity[32];
    static char s_remaining[32];
    static char s_rounds[32];

    // Read ticks from worker's packet
    snprintf(s_activity, sizeof(s_activity), settings.Name[currentInterval]);
    snprintf(s_remaining, sizeof(s_remaining), "%d", remainingSeconds);
    snprintf(s_rounds, sizeof(s_rounds), "%d", rounds);

    text_layer_set_text(s_first_line, s_activity);
    text_layer_set_text(s_second_line, s_remaining);
    text_layer_set_text(s_third_line, s_rounds);

    // Show to user in TextLayer
    if (remainingSeconds < 3)
    {
      vibes_double_pulse();
    }

    if (remainingSeconds == 1)
    {
      do
      {
        currentInterval = ((currentInterval + 1) % 3);
      } while (settings.Timer[currentInterval] == 0);
      remainingSeconds = settings.Timer[currentInterval];
      if (currentInterval == 0)
      {
        rounds++;
      }
    }
    else
    {
      remainingSeconds--;
    }
  }
}

static void select_click_handler(ClickRecognizerRef recognizer, void *context)
{
  // Check to see if the worker is currently active
  bool running = app_worker_is_running();
  remainingSeconds = settings.Timer[0];
  rounds = 0;
  currentInterval = 0;

  // Toggle running state
  AppWorkerResult result;
  if (running)
  {
    result = app_worker_kill();

    if (result == APP_WORKER_RESULT_SUCCESS)
    {
      reset_interval_display();
    }
    else
    {
      text_layer_set_text(s_second_line, "Error");
    }
  }
  else
  {
    result = app_worker_launch();

    if (result != APP_WORKER_RESULT_SUCCESS)
    {
      text_layer_set_text(s_first_line, "Error launching worker!");
    }
  }

  APP_LOG(APP_LOG_LEVEL_INFO, "Result: %d", result);
}

static void click_config_provider(void *context)
{
  window_single_click_subscribe(BUTTON_ID_SELECT, select_click_handler);
}

static void main_window_load(Window *window)
{
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);
  s_first_line = text_layer_create(GRect(0, 0, bounds.size.w, 50));

  layer_add_child(window_layer, text_layer_get_layer(s_first_line));

  s_second_line = text_layer_create(GRect(0, 50, bounds.size.w, 60));
  s_third_line = text_layer_create(GRect(0, 110, bounds.size.w, 60));

  text_layer_set_text_alignment(s_third_line, GTextAlignmentCenter);
  text_layer_set_text_alignment(s_first_line, GTextAlignmentCenter);
  text_layer_set_text_alignment(s_second_line, GTextAlignmentCenter);

  text_layer_set_font(s_first_line, fonts_get_system_font(FONT_KEY_BITHAM_42_BOLD));
  text_layer_set_font(s_third_line, fonts_get_system_font(FONT_KEY_BITHAM_42_BOLD));
  text_layer_set_font(s_second_line, fonts_get_system_font(FONT_KEY_BITHAM_42_BOLD));

  reset_interval_display();
  layer_add_child(window_layer, text_layer_get_layer(s_third_line));
  layer_add_child(window_layer, text_layer_get_layer(s_second_line));
}

static void reset_interval_display()
{
  text_layer_set_text(s_first_line, settings.Name[0]);
  static char s_one[32];
  static char s_two[32];
  snprintf(s_one, sizeof(s_one), "%d", settings.Timer[0]);
  snprintf(s_two, sizeof(s_two), "%d", 0);
  text_layer_set_text(s_second_line, s_one);
  text_layer_set_text(s_third_line, s_two);
}

static void main_window_unload(Window *window)
{
  // Destroy UI
  text_layer_destroy(s_first_line);
  text_layer_destroy(s_second_line);
}

static void init(void)
{
  prv_load_settings();

  // Listen for AppMessages
  app_message_register_inbox_received(prv_inbox_received_handler);
  app_message_open(128, 128);

  // Setup main Window
  s_main_window = window_create();
  window_set_background_color(s_main_window, GColorBlack);
  window_set_click_config_provider(s_main_window, click_config_provider);
  window_set_window_handlers(s_main_window, (WindowHandlers){
                                                .load = main_window_load,
                                                .unload = main_window_unload,
                                            });
  window_set_background_color(s_main_window, GColorScreaminGreen);
  window_stack_push(s_main_window, true);

  // Subscribe to Worker messages
  app_worker_message_subscribe(worker_message_handler);
}

static void deinit(void)
{
  // Destroy main Window
  window_destroy(s_main_window);

  rounds = 0;

  //gpath_destroy(s_house_path);

  // No more worker updates
  app_worker_message_unsubscribe();
}

int main(void)
{
  init();
  app_event_loop();
  deinit();
}
